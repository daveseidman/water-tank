// TODO: ability to pause teams?
// TODO: automatically start next team based on queue?

import EJS from 'ejs';
import Socket from './utils/Socket';
import Prompt from './utils/Prompt';
import { managerTemplate, addTeamTemplate } from './utils/Templates';

export default class Manager {
  constructor() {
    this.addTeam = this.addTeam.bind(this);
    this.listen = this.listen.bind(this);
    this.renderTeams = this.renderTeams.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'display';

    this.teams = null;
    this.title = document.createElement('h1');
    this.title.className = 'title';
    this.title.innerText = 'Game Manager';

    this.teamList = document.createElement('div');
    this.teamList.className = 'teams';

    this.footer = document.createElement('div');
    this.footer.className = 'footer';
    this.serverStatusText = document.createElement('p');

    this.serverStatusText.className = 'server-status disconnected';
    this.serverStatusText.innerText = 'Room Client:';
    this.addTeamButton = document.createElement('button');
    this.addTeamButton.className = 'add-team';
    this.addTeamButton.innerText = '+ ADD TEAM';
    this.footer.appendChild(this.addTeamButton);
    this.footer.appendChild(this.serverStatusText);

    this.element.appendChild(this.title);
    this.element.appendChild(this.teamList);
    this.element.appendChild(this.footer);

    this.socket = new Socket();

    this.listen();

    this.prompt = null;
    this.addingTeam = false;
    this.sortBy = 'name';
    this.sortAscending = true;

    fetch('/api/getTeams').then(res => res.json()).then((res) => {
      this.renderTeams(res);
    });
    fetch('/api/getServerStatus').then(res => res.json()).then((res) => {
      this.serverStatusText.innerHTML = `Room Client: <b>${res.status}</b>`;
    });

    window.editTeam = ({ target }) => {
      const nameField = target.parentNode.querySelector('.team-name');
      nameField.disabled = false;
      nameField.focus();
      nameField.selectionStart = nameField.value.length;
      nameField.selectionEnd = nameField.value.length;
    };

    window.resetTeam = ({ target }) => {
      this.prompt = new Prompt({
        message: 'Are you sure you want to reset this team?',
        responses: [
          {
            text: 'yes',
            callback: () => { this.socket.emit('resetTeam', { _id: target.parentNode.id }); },
          }, {
            text: 'no',
            callback: () => { console.log('delete cancelled'); },
          }],
      });
    };

    window.deleteTeam = ({ target }) => {
      this.prompt = new Prompt({
        message: 'Are you sure you want to delete this team?',
        responses: [
          {
            text: 'yes',
            callback: () => { this.socket.emit('deleteTeam', { _id: target.parentNode.id }); },
          }, {
            text: 'no',
            callback: () => { console.log('delete cancelled'); },
          }],
      });
    };

    window.startTeam = ({ target }) => {
      const { status, name } = this.teams[parseInt(target.parentNode.getAttribute('data-index'), 10)];
      if (status !== 'waiting') {
        this.prompt = new Prompt({
          message: `Team ${name} is not waiting to play`,
          responses: [
            {
              text: 'okay',
              callback: () => { console.log('ok'); },
            }],
        });
      } else this.socket.emit('startGame', { _id: target.parentNode.id });
    };

    window.teamEdited = ({ key, target }) => {
      if (key === 'Enter') {
        const index = parseInt(target.parentNode.getAttribute('data-index'), 10);
        target.disabled = true;
        // TODO: this may be brittle
        this.teams[index].name = target.value;
        this.socket.emit('updateTeam', this.teams[index]);
      }
    };

    window.sortTeams = (type) => {
      console.log('here', type);
      if (this.sortBy === type) this.sortAscending = !this.sortAscending;
      else this.sortBy = type;
      this.renderTeams({ teams: this.teams });
    };
  }

  renderTeams({ teams }) {
    // TODO: combine these two lines:
    if (this.sortAscending) teams.sort((a, b) => ((a[this.sortBy] > b[this.sortBy]) ? 1 : -1));
    else teams.sort((a, b) => ((a[this.sortBy] < b[this.sortBy]) ? 1 : -1));
    this.teams = teams;
    this.teamList.innerHTML = EJS.render(managerTemplate, { teams });
  }

  listen() {
    this.socket.on('teams', this.renderTeams);

    this.socket.on('error', (data) => {
      this.prompt = new Prompt({
        message: data.message,
        responses: [
          {
            text: 'okay',
            callback: () => { console.log('ok'); },
          }],
      });
    });

    this.socket.on('roomStatus', (data) => {
      this.serverStatusText.innerHTML = `Room Client: <b>${data.status}</b>`;
      this.serverStatusText.className = `server-status ${data.status}`;
    });
    this.addTeamButton.addEventListener('click', this.addTeam);
  }

  addTeam() {
    if (this.addingTeam) return;
    this.addingTeam = true;
    const team = {
      order: this.teams.length + 1,
    };

    this.teamList.innerHTML += EJS.render(addTeamTemplate, { team });
    const nameField = this.teamList.querySelector('.focus');
    console.log(nameField);
    // nameField.classList.remove('focus');
  }
}
