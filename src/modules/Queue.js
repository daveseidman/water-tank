// displays only teams with status = complete sorted by score
// no editing ability, just a disply
// should animate new teams on for effect

import EJS from 'ejs';
import Socket from './utils/Socket';
import { queueTemplate } from './utils/Templates';


export default class Queue {
  constructor() {
    this.renderTeams = this.renderTeams.bind(this);
    this.element = document.createElement('div');
    this.element.className = 'display';

    this.teamList = document.createElement('div');
    this.teamList.className = 'teams';

    this.title = document.createElement('h1');
    this.title.className = 'title';
    this.title.innerText = 'Game Queue';

    this.element.appendChild(this.title);
    this.element.appendChild(this.teamList);

    this.socket = new Socket();
    this.socket.on('teams', this.renderTeams);

    this.teams = null;
    this.prevTeams = null;

    fetch('/api/getTeams').then(res => res.json()).then((res) => {
      this.renderTeams(res);
    });
  }

  renderTeams({ teams }) {
    teams = teams.filter(team => team.status === 'waiting');
    teams.sort((a, b) => ((a.score < b.score) ? 1 : -1));
    this.teams = teams;
    if (this.prevTeams) {
      const newTeams = this.teams.filter(o => !this.prevTeams.some(v => v._id === o._id));
      const oldTeams = this.teams.filter(o => this.prevTeams.some(v => v._id === o._id));
      newTeams.forEach((team) => { team.new = true; });
      console.log(newTeams, oldTeams);
    }
    this.teamList.innerHTML = EJS.render(queueTemplate, { teams });
    this.prevTeams = this.teams;
  }
}
