import io from 'socket.io-client';

export default class Socket {
  constructor() {
    return io(':8000');
  }
}
