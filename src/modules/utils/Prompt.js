export default class Prompt {
  constructor(data) {
    this.element = document.createElement('div');
    this.element.className = 'prompt';

    const title = document.createElement('h1');
    title.innerText = data.message;

    const responses = document.createElement('div');
    data.responses.forEach((response) => {
      const button = document.createElement('button');
      button.innerText = response.text;
      button.addEventListener('click', () => {
        document.body.removeChild(this.element);
        response.callback();
      });

      responses.appendChild(button);
    });

    this.element.appendChild(title);
    this.element.appendChild(responses);

    document.body.appendChild(this.element);
  }
}
