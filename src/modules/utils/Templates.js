module.exports.managerTemplate = `
  <div class='teams-header'>
    <div class='team'>
      <div class='team-number' onclick='sortTeams("number")'>#</div>
      <input class='team-name' type='text' onclick='sortTeams("name")' value='Team'></input>
      <div class='team-status' onclick='sortTeams("status")'>Status</div>
      <div class='team-score' onclick='sortTeams("score")'>Score</div>
      <div class='team-edit'>Edit</div>
      <div class='team-reset'>Reset</div>
      <div class='team-delete'>Delete</div>
      <div class='team-start'>Start</div>
    </div>
  </div>
  <div class='teams-list'>
    <% for (var i = 0; i < teams.length; i++) { %>
    <div class='team' id='<%= teams[i]._id %>' data-index='<%= i %>'>
      <div class='team-number'><%= teams[i].order %></div>
      <input class='team-name' type='text' disabled=true onkeydown='teamEdited(event)' value='<%= teams[i].name %>'></input>
      <div class='team-status'><%= teams[i].status %></div>
      <div class='team-score'><%= teams[i].score %></div>
      <button class='team-edit' onclick='editTeam(event)'>✎</button>
      <button class='team-reset' onclick='resetTeam(event)'>↻</button>
      <button class='team-delete' onclick='deleteTeam(event)'>✖</button>
      <button class='team-start' onclick='startTeam(event)'>►</button>
    </div>
    <% }; %>
</div>`;

module.exports.addTeamTemplate = `
  <div class='team'>
    <div class='team-number'><%= team.order %></div>
    <input class='team-name focus' type='text' disabled=true onkeydown='teamEdited(event)'></input>
    <div class='team-status'>waiting</div>
    <div class='team-score'>0</div>
    <button class='team-edit'>✎</button>
    <button class='team-reset'>↻</button>
    <button class='team-delete'>✖</button>
    <button class='team-start'>►</button>
  </div>
`;

module.exports.leaderboardTemplate = `
  <div class='teams-header'>
    <div class='team'>
      <div class='team-name'>Team</div>
      <div class='team-score'>Score</div>
    </div>
  </div>
  <div class='teams-list'>
    <% for (var i = 0; i < teams.length; i++) { %>
    <div class='team' id='<%= teams[i]._id %>' data-index='<%= i %>'>
      <div class='team-name'><%= teams[i].name %></div>
      <div class='team-score'><%= teams[i].score %></div>
    </div>
    <% }; %>
  </div>`;


module.exports.queueTemplate = `
  <div class='teams-header'>
    <div class='team'>
      <div class='teams-team-number'>#</div>
      <div class='teams-team-name'>Team</div>
    </div>
  </div>
  <div class='teams-list'>
    <% for (var i = 0; i < teams.length; i++) { %>
    <div class='team <%= teams[i].new ? 'new' : '' %>' id='<%= teams[i]._id %>' data-index='<%= i %>'>
      <div class='team-number'><%= teams[i].order %></div>
      <div class='team-name'><%= teams[i].name %></div>
    </div>
  <% }; %>`;
