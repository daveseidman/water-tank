// displays only teams with status = complete sorted by score
// no editing ability, just a disply
// should animate new teams on for effect

import EJS from 'ejs';
import Socket from './utils/Socket';
import { leaderboardTemplate } from './utils/Templates';


export default class Leaderboard {
  constructor() {
    this.renderTeams = this.renderTeams.bind(this);
    this.element = document.createElement('div');
    this.element.className = 'display';

    this.teamList = document.createElement('div');
    this.teamList.className = 'teams';

    this.title = document.createElement('h1');
    this.title.className = 'title';
    this.title.innerText = 'Leaderboard';

    this.element.appendChild(this.title);
    this.element.appendChild(this.teamList);

    this.socket = new Socket();
    this.socket.on('teams', this.renderTeams);

    fetch('/api/getTeams').then(res => res.json()).then((res) => {
      this.renderTeams(res);
    });
  }

  renderTeams({ teams }) {
    if (this.teams) {
      const newTeams = teams.filter(o => !this.teams.some(v => v._id === o._id));
      console.log(newTeams);
    }
    teams = teams.filter(team => team.status === 'complete');
    teams.sort((a, b) => ((a.score < b.score) ? 1 : -1));
    this.teams = teams;
    this.teamList.innerHTML = EJS.render(leaderboardTemplate, { teams });
  }
}
