import io from 'socket.io-client';

const socket = io();

const debug = document.createElement('p');
debug.innerText = 'debug';
document.body.appendChild(debug);

const deviceOrientation = ({ alpha, beta, gamma }) => {
  socket.emit('orientation', { alpha, beta, gamma });
  debug.innerHTML = `${alpha.toFixed(1)},${beta.toFixed(1)},${gamma.toFixed(1)}`;
};

debug.addEventListener('click', () => {
  if (typeof DeviceOrientationEvent.requestPermission === 'function') {
    DeviceOrientationEvent.requestPermission()
      .then((permissionState) => {
        if (permissionState === 'granted') {
          window.addEventListener('deviceorientation', deviceOrientation);
        }
      })
      .catch(console.error);
  } else {
    window.addEventListener('deviceorientation', deviceOrientation);
  }
});
