const express = require('express');
const fs = require('fs');
const https = require('https');

const app = express();

const server = https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert'),
}, app).listen(8000, () => {
  console.log('server listening on 8000');
});
// const server = require('http').Server(app);
const io = require('socket.io')(server);

let unitySocket = null;

app.use(express.static('dist'));

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/dist/index.html`);
});

io.on('connection', (socket) => {
  console.log('connection', socket.id, socket.handshake.query);
  if (socket.handshake.query.unity) {
    unitySocket = socket;
  }

  // if (socket.handshake.query.unity) {
  // }

  socket.on('motion', (data) => {
    if (unitySocket) {
      io.to(unitySocket.id).emit('motion', data);
    }
    // console.log(`motion: ${data.x.toFixed(2)}, ${data.y.toFixed(2)}, ${data.z.toFixed(2)}`);
  });

  socket.on('orientation', ({ alpha, beta, gamma }) => {
    if (unitySocket) {
      io.to(unitySocket.id).emit('orientation', { alpha, beta, gamma });
    }
  });
});
